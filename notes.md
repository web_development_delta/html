# HTML(Hyper Text Markup Language)

## HTML INTRODUCTION

- used to add the structure and formatting in webpage.
- it helps browser to present the rules.

## HTML tags

- it is the container for some content or other tag.

```html
<p> This is the paragraph</p>
```

## Paragraph tag

- the ```html <p> ``` element represent a paragraph
- if we want to display the content in same then we use the paragraph tag.
- if we want  space then we need the to add the different <p> tag.
- it does not allow the extra space.


## nested tags

- add the tag inside the tag we called it as nested tags.

## Heading elements

- for heading there are 6 diffrent tags in html

```html
<h1> heading-1</h1>
<h2> heading-2</h2>
<h3> heading-3</h3>
<h4> heading-4</h4>
<h5> heading-5</h5>
<h6> heading-6</h6>

```
- we can't change the default size of the heading.
- there is only 1 h1 tag in the heading.beacause it use to show the heading.


## Boilerplate code

- the default html code is require to run the html file is we called as html boiler plate code.

```html
<!-- it represent we use html5 -->
<!DOCTYPE html> 
<!-- language is english -->
<!-- html is the root tag -->
<html lang="en">
    <!-- displays the hirarcay -->
    <!-- head comes first -->
    <!-- it contains the data about the data -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>page1</title>
</head>
<!-- body is the main part it has the main content display on web page -->
<body>
    
</body>
</html>


```

## List in HTML

- list is used to show the list content.
- there are two types of the lists
- 1. ordered list
- 2. unordered list

```html
<!--  type should be for order list : 'A','a','1' -->
<ol type = "1"> 
    <li></li>
    <li></li>
    <li></li>
    <li></li>

</ol>

<!-- Unorder list Type :  -->
<ul > 
    <li></li>
    <li></li>
    <li></li>
    <li></li>

</ul>

```

## Attributes in html

- attributes are used to add the more information about the tag.

``` html
<ol type = "1">
    <!-- here type gives more info about the order list tag so it is the attribute of the tag -->

```

## Anchor Elements

- used to add the links to the page.

``` html
<a href ="https://google.com">Google</a>
<!-- href is the attribute of the anchor tag -->
<!-- href : hypertext reference -->
<!-- Links : 1 absolute Link : like google,netflix links -->
<!-- 2. relative Links : file links :  -->


```

## Image Element

- use to add the image in your page.
- it is not the pair tag like heading,anchor.
- it is single tag. like br,hr
- this single tag does not contain the extra text. means like in anchor we provide the name Google in below example.
```html
<a href = "https://google.com" >Google</a>
```
  
``` html
<img src = "" alt = "">
<!-- src = source (link of the image (relative link)) -->
<!-- alt = alternate name : due to internet issue -->
<!-- we can use the absolute url in src to download image from internet -->
<!--  copy image address -->

```

## Br Tag

- use to break the line
- used to add the next line to the page.
- <br>

## Bold, Italic, Underline Tags

- used to highlight the text in your page.

```html
- <b> Bold </b>
- <i> Italic </i>
- <u> Underline </u>

```

## Comments in html

```html
<!-- this is the comments -->

```

## Is html is case sensitive

- Html is not case sensitive


## CHAPTER 2

## inline vs Block Elements

- the elements are classifies into two categories
- Inline element and block element

- 1. Block Element :Takes up the full width available(whole block)
-  start from the new line.
-  eg. h,p,body tags
-  2. Inline Element : Takes up only necessory width
-  don't start from the new line
-  img,anchor,etc.

## Div element

- also known as the content division element
- it acts like the container for the other elements.
- infact the website is the collection of the div elements.

- div is the element used to hold the other html elements or group of elements
- It is the block element.


## Span Element

- span is also a generic container used to hold the other html elements or group elements together.
- it is like the div element but the major difference is div is block element and span is inline element
- it is the inline element.
- when we never want to add the next line then we used the span element.
- eg Quora names in span .

## Hr Tag

- used to add the horizontal line in the webpage


## Sup and Sub Tags

- Sup is used to add the index number in square
- Sub is used to add the molecular formula in chmistry.

## Semantic Markup

- it is the markup that relates to the meaninng of the content.
- there are two types of the tags in html
- 1. sementic tag : we know its meaning by seeing them.
- eg. p, img,h,etc.
- 2. non-sementic tag : we don't know about the content by seeing them.
- their names does not match with the markup
- div,span,etc.


- Most of the good websites uses the sementic markup. 
- that is the good .
- Sementic markup that relates the meaning of the content.
- it makes the code meaningful,makes layout structure.
- as we apply only div then that makes us confusing.
- It makes website SEO(Search engine Optimaization)
- It helps browser to show the hirarcy of the website,Improves the rank.
- makes website more redable.
- Screen Readers experience gets improve.


## Tags in Sementic Markup


```html
<!-- Header tag Displays the header of the webpage -->

<header>Apna Collage<a href = "www.apnacollage.in">Go to main Page</a></header>

<!-- main tag displays the major part of the website -->
<main>This tag shows the main content</main>

<!-- footer tag  displays the last part of the website -->

<footer>made with &hearts</footer>

<!--  we make the one header ,main and footer in single webpage -->

<!-- Nav tag : used for navigation -->
used to navigate in navbar 
mostly contain the links
<nav></nav>

<!-- article tag :  -->

<article></article>

<!-- Section Tag : to group together related content -->
<!--  it is like div and span but it is the sementic tag -->
<section></section>


<!-- aside tag : related links content  -->
<!--  this are indirectly related topics -->
<aside></aside>
```

## HTML entities

- html entities are used to add the symbols,space and many other things in html
- An html is the piece of text (string) that begin with thw & and end with the (;)
- they are used to display the reversed characters (they are interpreted by html code)
- and invisible characters like non-breaking space
- can also used in characters that are diffecult to write with standard keyboard.
- browser interprets them and render correctly.


```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>page1</title>
</head>
<body>
    <h1>a&ltb</h1>
    <p>Sunbeam Infotech &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;pune</p> 
    <!-- to print the heart -->
    <h1>&hearts;</h1>

</body>
</html>


<!-- some commenly used entities
1. &   &amp;
2. <    &lt;
3. >    &gt;
4. =    &eqot

NOTE : for more html entities you can visit
> MDN html entities
-->
```


## HTML emmet

- visit emmet.io
- it is the essential toolkit for web developers. collection of the tools
- in vscode there is the emmet is available.
- 



## video tag

- video tag is used to add the video to the webpage.

```html
<video> this is he video tag</video>
<!-- it is the inline element and has different attributes like autoplay,mute,etc -->

```
- for more info about the video tag is visit : video MDN

## Understanding the html5

- doctype html represents the html5
- the html 5 is the buzzwork represent the mordern web technologies.
- it is like the latest version.
- it includes the new upgrades of html also have the 


## PART |||

## tables in HTML

- tables are used to represent the real life table data.
- table consist of row and columns as the attribute.
- row is horizintal
- column is vertical.
```html
<table border="1px solid black">
        <caption>table Caption</caption>
        <tr>
            <th>header 1</th>
            <th>header 2</th>
        </tr>
        <tr>
            <td>data1</td>
            <td>data2</td>
        </tr>
       </table>

```

## Semantic in table

- we use sementic tags in table
- they are starts with t.

```html
<thead></thead>   to wrap the table header
<tbody></tbody> to wrap the table body
<tfoot></tfoot> to wrap the table foot

```

## Rowspan and colspan attributes in table tag

- used to create the cells which spans over the multiple rows and columns.
```html
<h2>colspan and rowspan</h2>
       <table border="black">
        <thead>
            <tr>
                <th rowspan="2">Item</th>
                <th colspan="2">Price</th> 
            </tr>
            <tr><th>INR</th>
                <th>USD</th></tr>
        </thead>
       </table>

```
- while seeing the table check the rows and their colspan.
- and while seeing the column check the rowspan.
- another example
```html

<h2>practice question on table colspan and rowspan</h2>
       <table border="black">
        <caption>table with merged cell</caption>
        <thead>
            <tr>
                <th rowspan="2"></th>
                <th colspan="2">Average</th>
                <th rowspan="2">Red Eyes</th>
            </tr>
            <tr> 
                <th>height</th>
                <th>weight</th>  
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>males</td>
                <td>1.9</td>
                <td>0.003</td>
                <td>40%</td>
            </tr>
        </tbody>
       </table>


```

## Forms in html

- forms are used to collect the data from the user.
```html

<form>
    <!-- form content -->
</form>
```

- forms are comeup with the different attributes 


## action attributes

- action attribute is used to define what actions needs to perform when form is submitted.
- or where the form data should be present.


## input element

- imp element of the form tag.
- input create the multiple form controls.
- there are multiple input types of input that can be created using type attribute.

```html
<input type = 'text>
<input type = 'password>
<input type = 'email>
<input type = 'tel>
<input type = 'time>
<input type = 'color>

```

## placeholder attribute

- it is the attribute which see before entering the value in input tag.
- this helps in guiding the user what to write or give the example.

## label element

- label element represent the caption for an item in a user interface.
- 
```html
<label>
    Username :
    <input type  = "text" placeholder = "username">
</label>
<!-- this will for a connection between the username and input box
by clicking the username we can enter into the box -->
``

- classsical approach of writting an label
```html
<label for = "username"> Enter your username:</label>
<input type = "text" id = "username" placeholder = "username">
<!-- here id is the unique  attribute name given to the element -->

```
- there is for and id connection
- so if we have an id to input element then we can connect it with the label by using for



## button element

- button is the element which use to pass the information by action
- and when we declare the button inside the form element then we  are submitting that information to the file which is mentioned in the action attribute of the form tag.

- there are the different types of the buttons type
- type attribute
```html
<button type = "submit">submit</button>
<button type = "button">do something</button>
<button type = "reset">reset</button>

<!-- button using input tag and type is submit -->
<input type = "submit" value ="click me">
```

## name attribute in form elements

- name of the form control . Submit with the form of the name and value pair.
- 
```html
<input type = "text" id = "username" name = "user>

```

## Input elements

1. checkbox
- checkbox MDN
- we can checked multiple checkboxes.
```html
<input type="checkbox" name = "age" id="age" checked />
<label for ="age">I am 18+</label>
```
2. radio elements

- radio MDN
```html
<input type ="radio" name="fruit" id="apple" value="apple" />
<label for ="apple">Apple</label>
```
- we can select the one choice out from one.
- one button gets selected.
- we have to select one option.
- in this group name attribute should  be same.
- used in yes  no options
- gender
- or any.
- if we want to process the name and value
- then the value should pass the value.

## Select element

- used to make the dropdown
- 
```html

<label for="country">Choose your Country :</label>
    <select name="country" id="country">
        <option value="india">India</option selected>
        <option value="china">China</option>
        <option value="nepal">nepal</option>
        <option value="Bhutan">Bhutan</option>
        <option value="US">US</option>
        <option value="UK">UK</option>
    </select>
<!-- seleced default select it -->
```

## Range Element

- if we donot have the options to selects the we should provide the range input element
- for more search : range input MDN
``` html

<label for ="volume">Volume :</label>
<input type="range" min="0" max="100" name="vol" step = "10" value ="50" />
<!-- min and max are the range of the volume -->
<!--  and step is the in how many steps that volume gets covered. -->
<!-- value attribute gives the default start value of the range -->
```

## Text Area Element

- to take the text as the input.
- used in comments
```html
<label for="feedback"> Enter your valueable feedback</label>
<textarea id = "feedback"></textarea>

```

- we use the rows and cols attributes to increse the size.
- default size is 2 rows and 10 cols.
- we can change the size of the text-area.